<?php
/*
 * sites.drush.inc creates symlinks /var/aegir/sites to the domain directory.
 * This allows designers to setup their editor ( like Coda) to point to /var/aegir/sites/example.com. 
 * They do not need to know which platform or what the path to it is, allowing aegir to manage migrations
 * and upgrades freely.
 * 
 * TODO - Delete symlink on site deletion - http://drupal.org/node/1076202
 *
 * Implementation of hook_provision_apache_vhost_config()
 */
function sites_provision_apache_vhost_config($uri, $data) {

  // Add below lines to create symlink for site list directory
  $sites_directory = '/var/aegir/sites/';
  $site_path = $sites_directory . $uri;
  // drush_log('site_path = '. $site_path);
  if(is_link($site_path)) {
    // drush_log('unlinking '. $site_path);
    // unlink($site_path);
    provision_file()->unlink($site_path)
        ->succeed('Removed symlink for '. $site_path)
        ->fail('Could not remove symlink for '. $site_path);
  }
  // drush_log('linking '. $site_path);
  // symlink($site_publish_path,$site_path);  
  provision_file()->symlink(d()->root . '/sites/' . $uri, $site_path)
                          ->succeed('Created symlink for ', $uri)
                          ->fail('Could not create symlink for '. $uri); 
}
